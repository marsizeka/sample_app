class SessionsController < ApplicationController
  def new
  end

  def create
    # params [:session][:email] contains the email of the said session that
    # is tryng to login. The same can be said of the params [:session][:password]
    user = User.find_by(email: params[:session][:email].downcase)
    
    # auth via has_secure_password helper
    if user && user.authenticate(params[:session][:password])
      # Log the user in 
      log_in(user)
      # automatically remember the user with the code below
      #remember user # same as remember (user)
      # or, remember if the checkbox is clicked, thus getting 1 on the
      # params [][] hash value
      params[:session][:remember_me] == '1' ? remember(user) : forget(user)
      # and redirect to the user's show page.
      redirect_to user
    
    else
      # Redirect the user when the login failed
      # Flash the message only once with flash.now
      flash.now[:danger] = 'Invalid email/password combination' 
      render 'new'
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end

end