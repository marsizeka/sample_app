class UsersController < ApplicationController
  
  def show
    @user = User.find(params[:id])
   # debugger
  end
 
  def new
    @user = User.new
  end
  
  def create
      @user = User.new(user_params)
    if @user.save
      # log the new user automatically
      log_in @user # @user same as user?
      # Handle a successful save.
      flash[:success] = "Welcome to the Sample App!"
      redirect_to @user
    else
      # Handle a unsuccessful signup
      render 'new'
    end
  end


  private

    def user_params
    # mass assignement by using strong parametres
    # init a Ruby variable by using a hash of values
    # we require that the hash named params to have a attribute named user
    # and permit only name et al attributes to user attribute
    # an example of hash that contains hash values as well
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
    end
end