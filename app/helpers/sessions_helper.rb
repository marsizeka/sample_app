module SessionsHelper
    
    def log_in(user)
      # We treat session as if it were a hash, with key-pair values.
      # :user_id is the key, user.id is the value
    session[:user_id] = user.id
    end
    
    def current_user
      # Get the current user in order to facilitate the display of the app pages
      # based on the user logged on (that can be id-ed by session[:user_id]). 
      # The code below is the same as:
      # if @current_user.nil?
      # @current_user = User.find_by(id: session[:user_id])
      # else
      # @current_user
      # or
      # @current_user = @current_user || User.find_by(id: session[:user_id])
      # code from last chapter
      # @current_user ||= User.find_by(id: session[:user_id])
      # the code below updates current user taking into account the remember me feature
      if session[:user_id]
      @current_user ||= User.find_by(id: session[:user_id])
       elsif cookies.signed[:user_id]
      # raise # This code line causes the test to raise an exception. 
      # If tests pass, it means that this branch is currently untested.
            user = User.find_by(id: cookies.signed[:user_id])
        if user && user.authenticated?(cookies[:remember_token])
            log_in user
            @current_user = user
        end
      end
    end
    
  def remember(user) # the remember me feature. the real work is done here
    user.remember # use method from sessions_helper to generate token
    # Save its user_id and remember_token cookies
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end
  
  def log_out
      # logout process for the current user
    forget(current_user) # forget a permanent session/cookie
    session.delete(:user_id)
    @current_user = nil
    
  end
  
    def logged_in?
    # a helper method to enquire if said user is logged on or not
    !current_user.nil?
    end
  
  # Forgets a persistent session.
  def forget(user)
    user.forget # this code is equiv of update_attribute(:remember_digest, nil), thus
                # resetting the remember_token
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end

    
end
