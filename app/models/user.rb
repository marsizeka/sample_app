class User < ActiveRecord::Base
    
# the regular expression for valid email
VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

# remember digest that is used at the remember method, it is a symbol
attr_accessor :remember_token

before_save { self.email = email.downcase }

validates :name, presence: true, length: {maximum: 50}

validates :email, presence: true, length: {maximum: 255}, 
                    format: {with: VALID_EMAIL_REGEX},
                   uniqueness: true

has_secure_password

validates :password, presence: true, length: {minimum: 6}

# Returns the hash digest of the given string. To be used in testing
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

# Return the new token for the login process that has Remember me feature
  def User.new_token
    SecureRandom.urlsafe_base64
  end
  
# Associate a remember token with the user and saves the corresponding digest 
# in the db
  def remember
    self.remember_token = User.new_token # we use self. so that var can be global
    update_attribute(:remember_digest, User.digest(remember_token))
  end
  
# auth the remeber_token in the same way as has_secure_password feature
# for ad-hoc login by using bcrypt gem facilities
  def authenticated?(remember_token)
    return false if remember_digest.nil? # solve the bug of logging out in different browsera
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end
  
  # Forgets a user.
  def forget
    update_attribute(:remember_digest, nil)
  end
end