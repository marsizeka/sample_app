class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
  # create two other coloumns created_at and updated_at, 
  # which are timestamps that automatically record 
  # when a given user is created and updated
      t.timestamps null: false 
    end  
  end
end
