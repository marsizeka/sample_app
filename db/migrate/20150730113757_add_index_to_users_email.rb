class AddIndexToUsersEmail < ActiveRecord::Migration
  def change
    # add index for the email row of the users table, enforce uniqueness
       add_index :users, :email, unique: true
  end
end
